# jf361_ids721_mp10

 ## Project Introduction
This project involves implememting Rust Serverless Transformer Endpoint.

## Project Requirments
- Dockerize Hugging Face Rust transformer
- Deploy container to AWS Lambda
- Implement query endpoint


## Project Setup
#### 1. Implement the Lambda function
1. Create a Lambda function.
    ```angular2html
    cargo lambda new <PROJECT-NAME>
    cd <PROJECT-NAME>
   ```
2. Implement the function in `src/main.rs` and add dependencies in `Cargo.toml`.
#### 2. Dockerize the project
1. Create a new private repository in ECR
2. Login to AWS ECR using Docker
    ```angular2html
    aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <AWS-ACCOUNT-NUMBER>.dkr.ecr.us-east-1.amazonaws.com
    ```
3. Create and tag a Docker image, and then upload the Docker image to ECR
    ```angular2html
    docker buildx build --progress=plain --platform linux/arm64 -t <image-name> .
    docker tag <image-name>:latest <AWS-ACCOUNT-NUMBER>.dkr.ecr.us-east-1.amazonaws.com/mp10:latest
    docker push <AWS-ACCOUNT-NUMBER>.dkr.ecr.us-east-1.amazonaws.com/mp10:latest
    ```
#### 3. Deploy container to AWS Lambda
1. In `Lambda` console, create a new lambda function from `Container image`.
2. In `functional URL`, set `Auth type` to `None`, set `Invoke mode` to `RESPONSE_STREAM`, and enable `CORS`.


## Project Description
This project implements the function that could generate text.

cURL request against endpoint
```angular2html
curl -X POST https://ir6qzijuuv6n2zjvppwvjfa3au0kcxlg.lambda-url.us-east-1.on.aws/ -H "Content-Type: application/json" -d '{"text":"Cloud Computing for Data Analysis is important because"}'
```

## Screenshots
Run the project locally
<p> 
   <img src="screenshots/curl_loc.png" /> 
</p>
<p> 
   <img src="screenshots/curl_loc2.png" /> 
</p>

#### ERC image
<p> 
   <img src="screenshots/ERC.png" /> 
</p>

#### AWS Lambda
<p> 
   <img src="screenshots/lambda.png" /> 
</p>